<?php
/**
 * Template Name: HomePage
 * ==================================================
 * This is a template for Home Page
 *
 * @Theme Vertikal
 */
get_header();
/* ======================================================
   // Get Page Banner Information
   ====================================================== */
    
	// Get Breadcrumb Status 
	if ( is_front_page() ) {
		// We don't need it on frontpage
		$tmq_brcr = 'tmq_hide';
	} else {
		$tmq_brcr = ot_get_option( 'tmq_brcr' );
		if ( empty( $tmq_brcr ) ) {
			$tmq_brcr = 'tmq_show';
		}
	}
	
    // Get Banner Settings
	$tmq_slidertype = get_post_meta( $post->ID, 'tmq_slidertype', true );
	
	if ( empty( $tmq_slidertype ) ) {
		// Default value if nothing is set - Usually when theme is installed on a site with old content
		$tmq_banner_area_setting = ot_get_option( 'tmq_banner_area_setting' );
		if ( !empty( $tmq_banner_area_setting ) ) {
			// read from theme options
			$tmq_slidertype = $tmq_banner_area_setting;
		} else {
			// Set my favorite value as default
			$tmq_slidertype = 'tmq_text';
		}
	}
	
	if ( $tmq_slidertype == 'tmq_text' ) {

		// TEXT AND BACKGROUND is SET **********
		
		// Get page heading / title
		$tmq_banner_title = get_post_meta( $post->ID, 'tmq_banner_title', true );
		if ( empty( $tmq_banner_title ) ) {
			// Empty? Read from default and add span
			$tmq_banner_title = '<span>' . ot_get_option( 'tmq_default_banner_title' ) . '</span>';
		} else {
			// Add H1 tag to it
			$tmq_banner_title = '<h1>' . $tmq_banner_title . '</h1>';
		}
		
		// Get page sub-heading / sub-title
		$tmq_banner_subtitle = get_post_meta( $post->ID, 'tmq_banner_subtitle', true );
		if ( empty( $tmq_banner_subtitle ) ) {
			// Empty? Read from default
			$tmq_banner_subtitle = ot_get_option( 'tmq_default_banner_subtitle' );
		}
		
		// Get page background ( I Should Move This to Header )
		$tmq_banner_background_image = get_post_meta( $post->ID, 'tmq_banner_background_image', true );	
		
	} elseif ( $tmq_slidertype == 'tmq_revolution' ) {
		
		// REVOLUTION SLIDER is SET **********
		$tmq_revolution_slider = get_post_meta($post->ID, 'tmq_revolution_slider', true);
		
	} elseif ( $tmq_slidertype == 'tmq_flex' ) {
	
		// Flex Slider Images
		$tmq_flex_gallery = get_post_meta($post->ID, 'tmq_flex_gallery', true);
	}
?>
		<!-- content 
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div id="page-content">
				<!-- slider 
					================================================== -->
				<?php if ( $tmq_slidertype == 'tmq_text' ) { ?>
				<div id="page-banner">
					<?php echo $tmq_banner_title;?>
					<p><?php echo $tmq_banner_subtitle;?></p>
					<?php
						if ( ( function_exists( 'tmq_show_bc' ) ) && ( $tmq_brcr == 'tmq_show' ) ) {
								tmq_show_bc();
						}
					?>
				</div>
				<?php } ?>
				<?php if ( $tmq_slidertype == 'tmq_revolution' ) { ?>
				<div id="slider">
					<!--
					#################################
						- THEMEPUNCH BANNER -
					#################################
					-->
					<?php
						if ( function_exists( 'putRevSlider' ) ) {
							putRevSlider( $tmq_revolution_slider );
						}
					?>
				</div>
				<?php } ?>
				<?php if ( $tmq_slidertype == 'tmq_flex' ) { 
					// Check if it has more than one image to show it as slideshow
					if ( !empty( $tmq_flex_gallery ) ) {
						$tmq_flex_gallery_array = explode(',', $tmq_flex_gallery);
						?>
						<div id="slider">
							<!--
							#################################
									- FlexSlider Banner -
							#################################
							-->
							<div class="flexslider">
								<ul class="slides">
								<?php 
									foreach ( $tmq_flex_gallery_array as $tmq_flex_image ) {
										$img_title = get_the_title( $tmq_flex_image );   // title
										//$img_caption = get_post_field('post_excerpt', $tmq_flex_image ); // Get Caption - We don't use it now
										
										// Get slideshow size
										$big_array = image_downsize( $tmq_flex_image, 'full' );
										$img_url = $big_array[0];
										?>
											<li><img src="<?php echo esc_url($img_url); ?>" alt="" title="<?php echo esc_attr($img_title); ?>" /></li>
										<?php
									}
								?>
								</ul>
							</div>
						</div>
						<?php }
					} ?>
				<!-- End slider -->

				<!-- Content sections 
					================================================== -->
				<div class="content-sections">
					<?php
						/* ======================================================
						   // Get Sidebar Settings
						   ====================================================== */					
							$tmq_sidebar_position = get_post_meta( $post->ID, 'tmq_sidebar_position', true );
							if ( empty( $tmq_sidebar_position ) || $tmq_sidebar_position == 'tmq_default' ) {
								// Empty? Read from theme default
								$tmq_sidebar_position = ot_get_option( 'tmq_default_sidebar_position' );
							}
							if ( empty( $tmq_sidebar_position ) ) {
								// Even Theme Default is Empty?! OK! Force full width
								$tmq_sidebar_position = 'tmq_fullwidth';
							}
							
							// Load the layout based on theme options and page meta options
							switch ( $tmq_sidebar_position ) {
								case 'tmq_fullwidth':
									get_template_part( 'layouts/pages/no-sidebar', 'page' );
									break;
								case 'tmq_leftsidebar':
									get_template_part( 'layouts/pages/left-sidebar', 'page' );
									break;
								case 'tmq_rightsidebar':
									get_template_part( 'layouts/pages/right-sidebar', 'page' );
									break;
								default:
									get_template_part( 'layouts/pages/no-sidebar', 'page' );
									break;
							}
					?>
				</div>
			</div>
				<!-- End content sections -->
<?php get_footer(); ?>