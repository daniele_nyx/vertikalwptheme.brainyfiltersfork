<?php
/**
 * Template Name: Blog
 * This will use to create a blog page without using wordpress front-page settings
 *
 * @Theme Vertikal
 */
get_header();
/* ======================================================
   // Get Blog Banner Information
   ====================================================== */
    
	// Get Breadcrumb Status 
	$tmq_brcr = ot_get_option( 'tmq_brcr' );
	if ( empty( $tmq_brcr ) ) {
		$tmq_brcr = 'tmq_show';
	}
	
    // Get Banner Settings 
	if ( is_single() ) {
		$tmq_slidertype = get_post_meta( $post->ID, 'tmq_slidertype', true );
	} elseif ( is_home() ) {
		// it's default blog posts page (index.php)
		$tmq_slidertype = ot_get_option( 'tmq_blog_slidertype' );
		if ( empty ( $tmq_slidertype ) ) {
			$tmq_slidertype = 'tmq_text';
		}
		
		// We don't need breadcrumb on this page
		$tmq_brcr = 'tmq_hide';
		
	} else {
		// Archive / Category / Search and Tags
		$tmq_slidertype = '';
	}
	
	if ( empty( $tmq_slidertype ) ) {
		// Default value if nothing is set - Usually when theme is installed on a site with old content
		$tmq_banner_area_setting = ot_get_option( 'tmq_banner_area_setting' );
		if ( !empty( $tmq_banner_area_setting ) ) {
			// read from theme options
			$tmq_slidertype = $tmq_banner_area_setting;
		} else {
			// Set my favorite value as default
			$tmq_slidertype = 'tmq_text';
		}
	}	
		
	if ( $tmq_slidertype == 'tmq_text' ) {

		// TEXT AND BACKGROUND is SET **********
		
		// Get page heading / title
		if ( is_single() ) {
			// Posts
			$tmq_banner_title = get_post_meta( $post->ID, 'tmq_banner_title', true );
		} elseif ( is_home() ) {
			// Blog index
			$tmq_banner_title = ot_get_option( 'tmq_blog_banner_title' );
		} else {
			// Archive / Tag and ...
			$tmq_banner_title = '';
		}
		
		if ( empty( $tmq_banner_title ) ) {
			// Empty? Read from default and add span
			$tmq_banner_title = '<span>' . ot_get_option( 'tmq_default_banner_title' ) . '</span>';
		} else {
			// Add H1 tag to it
			$tmq_banner_title = '<h1>' . $tmq_banner_title . '</h1>';
		}
		
		// Get page sub-heading / sub-title
		if ( is_single() ) {
			// Posts
			$tmq_banner_subtitle = get_post_meta( $post->ID, 'tmq_banner_subtitle', true );
		} elseif ( is_home() ) {
			// Blog index
			$tmq_banner_subtitle = ot_get_option( 'tmq_blog_banner_subtitle' );
		} else {
			// Archive / Tag and ...
			$tmq_banner_subtitle = '';
		}
		if ( empty( $tmq_banner_subtitle ) ) {
			// Empty? Read from default
			$tmq_banner_subtitle = ot_get_option( 'tmq_default_banner_subtitle' );
		}
		
		// Get page background ( I Should Move This to Header )
		// $tmq_banner_background_image = get_post_meta( $post->ID, 'tmq_banner_background_image', true );	
		
	} elseif ( $tmq_slidertype == 'tmq_revolution' ) {
		
		// REVOLUTION SLIDER is SET **********
		if ( is_single() ) {
			// Posts
			$tmq_revolution_slider = get_post_meta($post->ID, 'tmq_revolution_slider', true);
		} elseif ( is_home() ) {
			// Blog index
			$tmq_revolution_slider = ot_get_option( 'tmq_blog_revolution_slider' );
		}
	} elseif ( $tmq_slidertype == 'tmq_flex' ) {
		
		// Flex Slider Images
		if ( is_single() ) {
			// Posts		
			$tmq_flex_gallery = get_post_meta($post->ID, 'tmq_flex_gallery', true);
		} elseif ( is_home() ) {
			// Blog index		
			$tmq_flex_gallery = ot_get_option( 'tmq_blog_flex_gallery' );
		}
	}
?>
		<!-- content 
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div id="page-content">			
				<!-- slider 
					================================================== -->
				<?php if ( $tmq_slidertype == 'tmq_text' ) { ?>
				<div id="page-banner">
					<?php echo $tmq_banner_title;?>
					<p><?php echo $tmq_banner_subtitle;?></p>
					<?php
						if ( ( function_exists( 'tmq_show_bc' ) ) && ( $tmq_brcr == 'tmq_show' ) ) {
								tmq_show_bc();
						}
					?>
				</div>
				<?php } ?>
				<?php if ( $tmq_slidertype == 'tmq_revolution' ) { ?>
				<div id="slider">
					<!--
					#################################
						- THEMEPUNCH BANNER -
					#################################
					-->
					<?php
						if ( function_exists( 'putRevSlider' ) ) {
							putRevSlider( $tmq_revolution_slider );
						}
					?>
				</div>
				<?php } ?>
				<?php if ( $tmq_slidertype == 'tmq_flex' ) { 
					// Check if it has more than one image to show it as slideshow
					if ( !empty( $tmq_flex_gallery ) ) {
						$tmq_flex_gallery_array = explode(',', $tmq_flex_gallery);
						?>
						<div id="slider">
							<!--
							#################################
									- FlexSlider Banner -
							#################################
							-->
							<div class="flexslider">
								<ul class="slides">
								<?php 
									foreach ( $tmq_flex_gallery_array as $tmq_flex_image ) {
										$img_title = get_the_title( $tmq_flex_image );   // title
										//$img_caption = get_post_field('post_excerpt', $tmq_flex_image ); // Get Caption - We don't use it now
										
										// Get slideshow size
										$big_array = image_downsize( $tmq_flex_image, 'full' );
										$img_url = $big_array[0];
										?>
											<li><img src="<?php echo esc_url($img_url); ?>" alt="" title="<?php echo esc_attr($img_title); ?>" /></li>
										<?php
									}
								?>
								</ul>
							</div>
						</div>
						<?php }
					} ?>
						<!-- End slider -->

				<!-- Content sections 
					================================================== -->
				<div class="content-sections">
					<?php
						/* ======================================================
						   // Get Sidebar Settings For Blog From Theme Options
						   ====================================================== */					
							$tmq_def_blog_sidebar_position = ot_get_option( 'tmq_def_blog_sidebar_position' );
							if ( empty( $tmq_def_blog_sidebar_position ) ) {
								// Empty? Force full width
								$tmq_def_blog_sidebar_position = 'tmq_fullwidth';
							}
							
							$tmq_sidebar_position = $tmq_def_blog_sidebar_position;
							
							// Check if it's a single blog post this way we should read from post meta data
							if ( is_single() ) {
								$tmq_sidebar_position = get_post_meta( $post->ID, 'tmq_sidebar_position', true );
								if ( empty( $tmq_sidebar_position ) || $tmq_sidebar_position == 'tmq_default' ) {
									// Empty? Read from theme default
									$tmq_sidebar_position = $tmq_def_blog_sidebar_position;
								}
							}
							
							// Load the layout based on theme options and post meta options
							switch ( $tmq_sidebar_position ) {
								case 'tmq_fullwidth':
									get_template_part( 'layouts/blog/no-sidebar', 'blog' );
									break;
								case 'tmq_leftsidebar':
									get_template_part( 'layouts/blog/left-sidebar', 'blog' );
									break;
								case 'tmq_rightsidebar':
									get_template_part( 'layouts/blog/right-sidebar', 'blog' );
									break;
								default:
									get_template_part( 'layouts/blog/no-sidebar', 'blog' );
									break;
							}
					?>
				</div>
			</div>
				<!-- End content sections -->
<?php get_footer(); ?>