<?php $searchquery = get_search_query();?>
<form class="searchbox" method="get" action="<?php echo esc_url(home_url());?>">
	<input placeholder="<?php _e('Search here...','vertikal')?>" id="site-search" type="search" name="s" value="<?php echo esc_attr($searchquery); ?>">
	<button type="submit">
		<i class="fa fa-search"></i>
	</button>
</form>