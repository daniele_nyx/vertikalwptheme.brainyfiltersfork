<?php
/**
 * Vertikal
 * ==================================================
 * This is the main function file - functions.php
 *
 */
 
  /* ==========================================================================
   Configure VC
   ========================================================================== */
   if ( !function_exists( 'tmq_enable_vc_post_types' ) ) {
	// Enable VC on all posts
		function tmq_enable_vc_post_types() {
			update_option('wpb_js_content_types', array( 'post', 'page', 'tmq-portfolio', 'tmq-magic-widget' ) );
		}
		add_action( 'after_setup_theme', 'tmq_enable_vc_post_types' );
	}
	if ( function_exists( 'wpb_js_composer_check_version_schedule_deactivation' ) ) {
		wpb_js_composer_check_version_schedule_deactivation(); 
	}
	
	/* Force adding VC Front CSS */
	function vc_enqueue_main_css_forever() { 
		wp_enqueue_style('js_composer_front'); 
	} 
	add_action('wp_enqueue_scripts', 'vc_enqueue_main_css_forever');
	
 /* ==========================================================================
   Loading Functions and Features
   ========================================================================== */

	include_once('includes/framework/ot.php');
	include_once('includes/framework/webfonts.php');
	include_once('includes/framework/breadcrumb.php');   
	include_once('includes/framework/authors.php');   
	include_once('includes/framework/menu.php');
	include_once('includes/framework/ajax-loader.php');
	include_once('includes/framework/enqueue.php');
	include_once('includes/framework/sidebars.php');   
	include_once('includes/framework/functions.php');
	include_once('includes/framework/theme-support.php');
	include_once('includes/framework/tgm.php');
	
	load_template( trailingslashit( get_template_directory() ) . 'includes/theme-options.php' );
	load_template( trailingslashit( get_template_directory() ) . 'includes/framework/meta-boxes.php' );

	// Add Image Sizes
	load_template( trailingslashit( get_template_directory() ) . 'includes/framework/images.php' );

 /* ==========================================================================
   Vertikal Self Error Checking
   ========================================================================== */
	if ( !function_exists( 'my_custom_warning' ) ) {
		function my_custom_warning() {
		  if( ot_get_option('tmq_css') != '{{tmq_color_settings}}' ) {
			echo '<div id="my-custom-warning" class="error"><h3>Warning: Vertikal Theme Activation</h3><p>There is a problem in your theme installation. Please follow these steps to fix it easily: </p><ul><li>If you\'ve installed the theme just now, You should go to your "<strong>Appearance > Theme Options</strong>" page and save your settings once and check if it\'s fixed.</li><li>Re-import your theme options in "<strong>Appearance > Options Backup</strong>". Then save your theme options once again.</li><li>If none of the above steps helped, then please contact support and we will help you on this issue soon.</li></ul></div>';
		  }
		}
		add_action( 'admin_notices', 'my_custom_warning' );	
	}
   

// Localisation Support
load_theme_textdomain('vertikal', get_template_directory() . '/languages');

// wordpress root directory
if ( !function_exists( 'get_wp_path' ) ) {
	function get_wp_path() {
		return str_replace( '\\', '/', ABSPATH );
	}
}

// display favorite icon
if ( !function_exists( 'show_favicon' ) ) {
	function show_favicon() {
		$favicon_url = ot_get_option( 'tmq_favicon' );
		if ( empty( $favicon_url ) ) {
			return false;
		}
		
		$favicon_dir = str_replace( get_site_url(), get_wp_path(), $favicon_url );
		if ( file_exists( $favicon_dir ) ) {
			echo '<link rel="shortcut icon" href="' . esc_url($favicon_url) . '">';
		} else {
			echo '<!-- No FavIcons Found -->';
		}
	}
	add_action('wp_head', 'show_favicon'); // Add Favorites Icon to Site
}

// display appletouch icon
if ( !function_exists( 'show_appletouchicon' ) ) {
	function show_appletouchicon() {
		$appletouchicon_url = ot_get_option( 'tmq_appletouchicon' );
		if ( empty( $appletouchicon_url ) ) {
			return false;
		}
		
		$appletouchicon_dir = str_replace( get_site_url(), get_wp_path(), $appletouchicon_url );
		if ( file_exists( $appletouchicon_dir ) ) {
			echo '<link rel="apple-touch-icon" href="' . esc_url($appletouchicon_url) . '"/>';
		} else {
			echo '<!-- No AppleTouchIcon Found -->';
		}
	}
	add_action('wp_head', 'show_appletouchicon'); // Add AppleTouch Icon to Site
}

// display edit button for admin
if ( !function_exists( 'show_tmq_admin_edit' ) ) {
	function show_tmq_admin_edit() {
		if ( current_user_can('update_themes') && current_user_can('edit_pages') && current_user_can('edit_posts')  ) {
			edit_post_link( '<i title="'. __( 'Edit Post', 'vertikal' ) .'" class="fa fa-pencil"></i>', '<span class="tmq-edit-link">', '</span>' );
		}
	}
	add_action('wp_footer', 'show_tmq_admin_edit'); // Add Edit This Icon for Admins
}

/* ==========================================================================
   Customize WP Functions
   ========================================================================== */
   
	// Custom Read More Content to Post
	if ( !function_exists( 'remove_read_more' ) ) {
		function remove_read_more( $more ) {
			global $post;
			return ' [...]';
		}
		add_filter('excerpt_more', 'remove_read_more'); 
	}
	// Remove Admin bar
	if ( !function_exists( 'remove_admin_bar' ) ) {
		function remove_admin_bar() {
			if ( ! current_user_can('administrator') && ! is_admin() )
				return false;
			else
				return false; //?!?@! what? this should be true for admin menu
		}
		// add_filter('show_admin_bar', 'remove_admin_bar'); 
	}
	
	// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
	if ( !function_exists( 'remove_thumbnail_dimensions' ) ) {
		function remove_thumbnail_dimensions( $html )
		{
			$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
			return $html;
		}
		add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); 
	}
	
/* ==========================================================================
   Add Filters for Above Functions
   ========================================================================== */
	
	// Allow shortcodes in Dynamic Sidebar
	add_filter('widget_text', 'do_shortcode'); 

	// Remove <p> tags in Dynamic Sidebars (better!)
	add_filter('widget_text', 'shortcode_unautop'); 	


/* ==========================================================================
   Get number of comments of a post
   ========================================================================== */
   if ( !function_exists( 'tmq_get_comments_count' ) ) {
		function tmq_get_comments_count( $post_id, $nocomments , $onecomment, $morecomments ) {
			$comments_count = get_comments_number( $post_id );
			if ( $comments_count == 0 || empty( $comments_count ) ) {
				return $nocomments;
			} elseif ( $comments_count == 1 ) {
				return '1 ' . $onecomment;
			} elseif ( $comments_count > 1 ) {
				return $comments_count . ' ' . $morecomments;
			} else {
				return '';
			}
		}
	}
/* ==========================================================================
	WooCommerce Support
	========================================================================== */

add_theme_support( 'woocommerce' );

/* ==========================================================================
	Area Widget Sub Footer nyx
	========================================================================== */

if ( function_exists('register_sidebar') ) {
    register_sidebar( array(
     'name' => __( 'Menu Footer'),
     'id' => 'menusubfooter',
     'description' => __( 'Menu Sub Footer',
     'vertikal' ),
     'before_widget' => '<div class="menu-footer">',
     'after_widget' => "</div>",
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
   ) );
}

/* ==========================================================================
  Area Widget Header Menù Language nyx
  ========================================================================== */

if ( function_exists('register_sidebar') ) {
    register_sidebar( array(
     'name' => __( 'Menu Language'),
     'id' => 'menulanguage',
     'description' => __( 'Menu Language',
     'vertikal' ),
     'before_widget' => '<div class="menu-language">',
     'after_widget' => "</div>",
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
   ) );
}

/* ======= NYX FILTERS AND ACTION OVERRIDES ======== */

// we don't want title in pages
add_filter( 'woocommerce_show_page_title', function() { return false; } );

// customize woocommerce bradcrumbs
function woocommerce_breadcrumb( $args = array() )
{
  if(is_search())
    return;

  $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
          'delimiter'   => '<i class="fa fa-arrow-circle-right"></i>',
          'wrap_before' => '<ul class="breadcrumb brcr-top">',
          'wrap_after'  => '</ul>',
          'before'      => '<li>',
          'after'       => '</li>',
          'home'        => 'Catalogo'
          ) ) );

  $breadcrumbs = new WC_Breadcrumb();

  if ($args['home'])
    $breadcrumbs->add_crumb($args['home'], get_permalink(9947)); // dirty code: 9947 is the id of the catalogo root page

  $args['breadcrumb'] = $breadcrumbs->generate();
  wc_get_template( 'global/breadcrumb.php', $args );
}

// hide empty categories from the categories tree
add_filter('woocommerce_product_categories_widget_args', function ($cat_args) {
  $cat_args['hide_empty'] = 1;
  return $cat_args;
});

// hide the tab reviews in product pages
add_filter( 'woocommerce_product_tabs', function ($tabs) {
  unset($tabs['reviews']);
  return $tabs;
});

// we need search by product tags and attribute terms: customize where clause including them
add_filter( 'posts_search', 'nyx_product_tags_and_attributes_search', 500, 2 );
function nyx_product_tags_and_attributes_search( $original_where, &$wp_query )
{
  if(!isset($wp_query->query_vars['wc_query']) || $wp_query->query_vars['wc_query'] != 'product_query')
    return $original_where;

  $new_where = get_new_where_clause_including_product_tags_search($original_where);
  $new_where .= get_attribute_terms_where_clause();
  return $new_where;
}

function get_new_where_clause_including_product_tags_search($original_where)
{
  if (empty($original_where))
    return $original_where;

  global $wpdb;
  $dbprefix = $wpdb->prefix;

  $terms = explode(' ', read_get_param('s'));
  if( $terms === FALSE || count( $terms ) == 0 )
    $terms = array( 0 => $terms );

  $new_where = '';
  foreach( $terms as $tag )
  {
    $tagpattern = "%".$tag."%";
    $new_where .= " AND (
      ({$dbprefix}posts.post_title LIKE '$tagpattern')
      OR ({$dbprefix}posts.post_content LIKE '$tagpattern')
      OR EXISTS
      (
       SELECT * FROM {$dbprefix}terms
       INNER JOIN {$dbprefix}term_taxonomy
       ON {$dbprefix}term_taxonomy.term_id = {$dbprefix}terms.term_id
       INNER JOIN {$dbprefix}term_relationships
       ON {$dbprefix}term_relationships.term_taxonomy_id = {$dbprefix}term_taxonomy.term_taxonomy_id
       WHERE taxonomy = 'product_tag'
       AND object_id = {$dbprefix}posts.ID
       AND {$dbprefix}terms.name LIKE '$tagpattern'
      )
      )";
  }

  return $new_where;
}
	
function get_attribute_terms_where_clause()
{
  $terms_ids = array_filter(
      array(
        read_get_param('raccordo'),
        read_get_param('funzionalita'),
        read_get_param('materiale'),
        read_get_param('tipologia'),
        read_get_param('applicazione'),
        ),
      'is_str_positive_int'
      );

  if(empty($terms_ids))
    return '';

  global $wpdb;
  $dbprefix = $wpdb->prefix;

  $clause = '';
  foreach($terms_ids as $id) {
    $clause .= " AND EXISTS (
      select *
      from {$dbprefix}term_taxonomy tt
      inner join {$dbprefix}term_relationships tr
      on tr.term_taxonomy_id = tt.term_taxonomy_id
      where tr.object_id = {$dbprefix}posts.ID AND tt.term_id = $id
      )";
  }

  return $clause;
}

function read_get_param($str) {
  return (isset($_GET[$str]) ? $_GET[$str] : '');
}

function is_str_positive_int($str) {
  return preg_match('/^\d+$/', $str);
}

function get_product_search_form( $echo = true  )
{
  // retrieve from db attributes possibile values
  $raccordi     = get_terms('pa_raccordo', 'orderby=name&hide_empty=1' );
  $funzionalita = get_terms('pa_funzionalita', 'orderby=name&hide_empty=1' );
  $materiali    = get_terms('pa_materiale', 'orderby=name&hide_empty=1' );
  $tipologie    = get_terms('pa_tipologia', 'orderby=name&hide_empty=1' );
  $applicazioni = get_terms('pa_applicazione', 'orderby=name&hide_empty=1' );

  $map = nyx_retrieve_product_attributes_map();

  ob_start();
  do_action('pre_get_product_search_form');

  wc_get_template('product-searchform.php', compact(
    'raccordi',
    'funzionalita',
    'materiali',
    'tipologie',
    'applicazioni',
    'map'
  ));

  $form = apply_filters( 'get_product_search_form', ob_get_clean() );

  if ( $echo ) {
    echo $form;
  } else {
    return $form;
  }
}

function nyx_retrieve_product_attributes_map() {

  global $wpdb;
  $dbprefix = $wpdb->prefix;

  $mysqlquery = "select distinct GROUP_CONCAT(tt.term_id ORDER BY tt.term_id) as terms
    from {$dbprefix}term_relationships tr
      inner join {$dbprefix}term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id 
      inner join {$dbprefix}terms t on tt.term_id = t.term_id
    where tt.taxonomy like 'pa_%'
    group by tr.object_id";

  $result = $wpdb->get_results($mysqlquery);
  $result = array_map(function($row) {
      return explode(',', $row->terms);
    }, $result);

  return json_encode($result);
}

