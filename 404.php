<?php
/**
 * Vertikal
 * ==================================================
 * This is the template to show normal pages
 *
 */
get_header();
/* ======================================================
   // Get 404 Page Settings
   ====================================================== */
    
	$tmq_404_heading = ot_get_option( 'tmq_404_heading' );
	if ( empty( $tmq_404_heading ) ) {
		$tmq_404_heading = __('404 File Not Found!', 'vertikal');
	}
	$tmq_404_text = ot_get_option( 'tmq_404_text' );
	if ( empty( $tmq_404_text ) ) {
		$tmq_404_text = __('Oops! The page you are looking for could not be found! Maybe you want to search for it?', 'vertikal');
	}
	$tmq_404_search = ot_get_option( 'tmq_404_search' );
	if ( empty( $tmq_404_search ) ) {
		$tmq_404_search = 'on';
	}

	
?>
		<!-- content 
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div id="page-content">

				<!-- Content sections 
					================================================== -->
				<div class="content-sections">
					<div class="err-404-box text-center">
						<h1 class="head-404"><?php echo $tmq_404_heading;?></h1>
						<p class="text-404"><?php echo $tmq_404_text;?></p>
						<?php
						if ( 'on' == $tmq_404_search ) {
						?>
						<div class="widget footer-widgets widget_search"><form class="searchbox" method="get" action="http://localhost/vertikal">
							<?php get_search_form(); ?>
						</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
				<!-- End content sections -->
<?php get_footer(); ?>