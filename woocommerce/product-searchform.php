<?php

wp_enqueue_script('woocommerce-nyx-product-searchform',
  (get_template_directory_uri() . '/woocommerce/js/nyx-product-searchform.js'),
  array('jquery')
);

if(!function_exists('nyx_echo_select')) {
  function nyx_echo_select($title, $name, $terms) {
    echo "<select name='$name'>";
    echo "  <option value=''>Scegli $title</option>";

    foreach($terms as $term) {
      echo "<option value='$term->term_id'";
      if(isset($_GET[$name]) && ($_GET[$name] == $term->term_id))
        echo " selected";
      echo ">$term->name</option>";
    }

    echo "</select>";
  }
}

?>

<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
  <script type="text/javascript">
    if (undefined === nyx_product_attributes_map) {
      var nyx_product_attributes_map = <?php echo $map; ?>;
    }
  </script>
  <div class="widget_search cerca">
    <input type="search" class="search-field" name="s" placeholder="Cerca per codice"
      value="<?php echo get_search_query(); ?>"
      title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>"
    />
    <br /><br />

    <?php nyx_echo_select('Raccordo', 'raccordo', $raccordi); ?>
    <?php nyx_echo_select('Funzionalità valvola', 'funzionalita', $funzionalita); ?>
    <?php nyx_echo_select('Materiale Corpo', 'materiale', $materiali); ?>
    <?php nyx_echo_select('Tipologia', 'tipologia', $tipologie); ?>
    <?php nyx_echo_select('Applicazione', 'applicazione', $applicazioni); ?>

    <input type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>" />
    <input type="hidden" name="post_type" value="product" />
  </div>
</form>
