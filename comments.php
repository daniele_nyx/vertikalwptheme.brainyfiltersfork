<?php
/**
 * Vertikal
 * ==================================================
 * This is the show comments template - comments.php
 *
 */
?>
		<?php
			// Don't show comments that are protected!
			if ( post_password_required() ) { ?>
				<div class="comment-section">			
					<p><?php _e( 'Post is password protected. Enter the password to view any comments.', 'vertikal' ); ?></p>
				</div> 
				<?php
				return; 
			} 
		?>
		<?php 
			if ( have_comments() ) { ?>
				<div class="comment-section">
					<h3><?php comments_number(); ?></h3>
					<ul class="comment-tree">
						<?php wp_list_comments('type=comment&callback=tmq_show_comment&style=li&avatar_size=60'); ?>								
					</ul>
				</div>
				<div class="row comment-section">
					<div class="col-md-6"><?php previous_comments_link();?></div>
					<div class="col-md-6"><?php next_comments_link();?></div>
				</div>
			<?php 
			} elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) { ?>
				<div class="comment-section">
					<p><?php _e( 'Comments are closed here.', 'vertikal' ); ?></p>
				</div>
		<?php
			}
			//return;
			
			// Change default comment form
			$comments_arg = array(
				'label_submit'	=> __( 'Leave a Comment', 'vertikal' ),
				'logged_in_as' => '<div class="row">
										<div class="col-md-12">
											<p class="logged-in-as">' .
					sprintf(
					__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
					  admin_url( 'profile.php' ),
					  $user_identity,
					  wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
					) . '</p>',
				'title_reply'	=> __( 'Leave a comment', 'vertikal' ),
				'fields'		=> apply_filters( 'comment_form_default_fields', 
					array(
						'author'	=> '<div class="row">
											<div class="col-md-4">
												<input type="text" id="author" name="author" placeholder="' . __( 'name', 'vertikal' ) . '"/>',
						'email'		=> '		<input type="text" id="email" name="email" placeholder="' . __( 'email', 'vertikal' ) . '"/>	',
						'url'		=> '		<input type="text" id="url" name="url" placeholder="' . __( 'website', 'vertikal' ) . '"/>
											</div>
											<div class="col-md-8">'
					)
				),
				'comment_field' =>  '			<textarea id="comment" name="comment" placeholder="' . __( 'message', 'vertikal' ) . '"></textarea>
												<!--input type="submit" name=submit" id="submit" value="'. __( 'Add comment', 'vertikal' ) .'"/-->
											</div>
										</div>',
			    'comment_notes_before' => '',

				'comment_notes_after' => '',					
			);
			ob_start();
			comment_form($comments_arg); 
			$comment_form = ob_get_clean();
			
			// Replace default submit button with nothing ( We've added ours previously )
			$comment_form = str_replace( '<input name="submit" type="submit" id="submit" value="'. __('Leave a Comment','vertikal') .'" />', '', $comment_form );
			echo $comment_form;
		?>