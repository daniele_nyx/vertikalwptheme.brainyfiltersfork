(function($) {

  "use strict";

  $(document).ready(function() {

    reloadProductFiltersOptions('.woocommerce-product-search');

    $('.woocommerce-product-search select').change(function() {
      reloadProductFiltersOptions($(this).parent());
    });

  });

  function reloadProductFiltersOptions(containerSelector) {

    var $container = $(containerSelector);
    var selectedTerms = readSelectedTerms($container);

    if(selectedTerms.length == 0) {
      showEveryOption($container);
      return;
    }

    $container.find('select[name]').each(function() {
 
      var currentSelectedTerms = selectedTermsWithoutCurrentSelect(selectedTerms, $(this));
      var allowedTermsSet = createAllowedTermsSet(nyx_product_attributes_map, currentSelectedTerms);

      $(this).find('option[value!=""]').each(function() {
          
        if(arrayContains(allowedTermsSet, $(this).val())) {
          showOptionElement($(this));
          return;
        }
        
        hideOptionElement($(this));
      });

    });

  }
  
  function readSelectedTerms($container) {
    return $container
      .find('select[name] option:selected[value!=""]')
      .map(function() { return $(this).val(); })
      .toArray();
  }

  function showEveryOption($container) {
    $container.find('select[name] option').each(function() {
      showOptionElement($(this));
    });
  }

  function selectedTermsWithoutCurrentSelect(selectedTerms, $selectElement) {
    var result = selectedTerms.slice();  // is just a clone
    var selectedOption = $selectElement.find('option:selected[value!=""]').first();

    if(selectedOption.length > 0) { 
      var indexOfSelectedValue = selectedTerms.indexOf(selectedOption.val());
      if(indexOfSelectedValue > -1)
        result.splice(indexOfSelectedValue, 1);
    }

    return result;
  }

  function createAllowedTermsSet(map, selectedTerms) {

    var arraysWithEverySelectedTerms = $.grep(map, function(array) {
      return arrayHasEveryValues(array, selectedTerms);
    });

    return mergeArrayOfArraysAndRemoveDuplicates(
      arraysWithEverySelectedTerms
    );
  }

  function mergeArrayOfArraysAndRemoveDuplicates(array) {
    return [].concat
      .apply([], array)
      .reduce(function(p, c) {
        if (!arrayContains(p, c)) p.push(c);
        return p;
      }, []);
  }

  function arrayContains(array, element) {
    return (array.indexOf(element) > -1);
  }

  function arrayHasEveryValues(searchIn, values) {
    for (var i = 0, len = values.length; i < len; i++) {
      if(!arrayContains(searchIn, values[i]))
        return false;
    }

    return true;
  }

  function showOptionElement($option) {
    if($option.parent('span.toggleOption').length)
      $option.unwrap();
  }

  function hideOptionElement($option) {
    if($option.parent('span.toggleOption').length)
      return;

    $option.wrap('<span class="toggleOption" style="display: none;" />');
  }

})(jQuery);
