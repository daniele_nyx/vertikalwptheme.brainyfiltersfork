<?php
/**
 * Vertikal
 * ==================================================
 * This is the search results page - search.php
 *
 */
get_header();
/* ======================================================
   // Get Blog Banner Information
   ====================================================== */
	
    // Get Banner Settings 
	$tmq_slidertype = ot_get_option( 'tmq_search_slidertype' );
	if ( empty ( $tmq_slidertype ) ) {
		$tmq_slidertype = 'tmq_none';
	}
		
	// We don't need breadcrumb on this page
	$tmq_brcr = 'tmq_show';
		
	if ( $tmq_slidertype == 'tmq_text' ) {

		// TEXT AND BACKGROUND is SET **********
		
		// Get page heading / title
		$tmq_banner_title = ot_get_option( 'tmq_search_banner_title' );
		
		if ( empty( $tmq_banner_title ) ) {
			// Empty? Read from default and add span
			$tmq_banner_title = '<span>' . ot_get_option( 'tmq_default_banner_title' ) . '</span>';
		} else {
			// Add H1 tag to it
			$tmq_banner_title = '<h1>' . $tmq_banner_title . '</h1>';
		}
		
		// Get page sub-heading / sub-title
		$tmq_banner_subtitle = ot_get_option( 'tmq_search_banner_subtitle' );

		if ( empty( $tmq_banner_subtitle ) ) {
			// Empty? Read from default
			$tmq_banner_subtitle = ot_get_option( 'tmq_default_banner_subtitle' );
		}
		
	} elseif ( $tmq_slidertype == 'tmq_revolution' ) {
		
		// REVOLUTION SLIDER is SET **********
		$tmq_revolution_slider = ot_get_option( 'tmq_search_revolution_slider' );
		
	} elseif ( $tmq_slidertype == 'tmq_flex' ) {
		
		// Flex Slider Images
		$tmq_flex_gallery = ot_get_option( 'tmq_search_flex_gallery' );
	}
?>
		<!-- content 
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div id="page-content">			
				<!-- slider 
					================================================== -->
				<?php if ( $tmq_slidertype == 'tmq_text' ) { ?>
				<div id="page-banner">
					<?php echo $tmq_banner_title;?>
					<p><?php echo $tmq_banner_subtitle;?></p>
					<?php
						if ( ( function_exists( 'tmq_show_bc' ) ) && ( $tmq_brcr == 'tmq_show' ) ) {
								tmq_show_bc();
						}
					?>
				</div>
				<?php } ?>
				<?php if ( $tmq_slidertype == 'tmq_revolution' ) { ?>
				<div id="slider">
					<!--
					#################################
						- THEMEPUNCH BANNER -
					#################################
					-->
					<?php
						if ( function_exists( 'putRevSlider' ) ) {
							putRevSlider( $tmq_revolution_slider );
						}
					?>
				</div>
				<?php } ?>
				<?php if ( $tmq_slidertype == 'tmq_flex' ) { 
					// Check if it has more than one image to show it as slideshow
					if ( !empty( $tmq_flex_gallery ) ) {
						$tmq_flex_gallery_array = explode(',', $tmq_flex_gallery);
						?>
						<div id="slider">
							<!--
							#################################
									- FlexSlider Banner -
							#################################
							-->
							<div class="flexslider">
								<ul class="slides">
								<?php 
									foreach ( $tmq_flex_gallery_array as $tmq_flex_image ) {
										$img_title = get_the_title( $tmq_flex_image );   // title
										//$img_caption = get_post_field('post_excerpt', $tmq_flex_image ); // Get Caption - We don't use it now
										
										// Get slideshow size
										$big_array = image_downsize( $tmq_flex_image, 'gallery-post-format' );
										$img_url = $big_array[0];
										?>
											<li><img src="<?php echo esc_url($img_url); ?>" alt="" title="<?php echo esc_attr($img_title); ?>" /></li>
										<?php
									}
								?>
								</ul>
							</div>
						</div>
						<?php }
					} ?>
				<!-- End slider -->

				<!-- Content sections 
					================================================== -->
				<div class="content-sections">
					<?php
						/* ======================================================
						   // Get Sidebar Settings For Blog From Theme Options
						   ====================================================== */					
							$tmq_search_sidebar_position = ot_get_option( 'tmq_search_sidebar_position' );
							if ( empty( $tmq_search_sidebar_position ) ) {
								// Empty? Force full width
								$tmq_search_sidebar_position = 'tmq_rightsidebar';
							}
							
							$tmq_sidebar_position = $tmq_search_sidebar_position;
							
							// Load the layout based on theme options and post meta options
							switch ( $tmq_sidebar_position ) {
								case 'tmq_fullwidth':
									get_template_part( 'layouts/blog/no-sidebar', 'blog' );
									break;
								case 'tmq_leftsidebar':
									get_template_part( 'layouts/blog/left-sidebar', 'blog' );
									break;
								case 'tmq_rightsidebar':
									get_template_part( 'layouts/blog/right-sidebar', 'blog' );
									break;
								default:
									get_template_part( 'layouts/blog/no-sidebar', 'blog' );
									break;
							}
					?>
				</div>
			</div>
				<!-- End content sections -->
<?php get_footer(); ?>
