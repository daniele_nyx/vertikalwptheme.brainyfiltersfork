<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>">

		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			// NYX:DM 30.03.2015 moved down product thumbnail
			//do_action( 'woocommerce_before_shop_loop_item_title' );
		?>

		<h3><?php the_title(); ?></h3>
		<?php
			// NYX:DM 30.03.2015 custom product list: thumbnail and attributes list after title
			//echo var_export($product, true);

      echo "<div class='product-thumbnail-wrapper'>";
			echo woocommerce_get_product_thumbnail('thumbnail');
      echo "</div>";
			
			$attributes = $product->get_attributes();
			foreach ( $attributes as $attribute ) :
				if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) )
					continue;
				
				$attributeTitle = wc_attribute_label( $attribute['name'] );
				$values = ($attribute['is_taxonomy'] ) ?
					wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) ) :
					array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );

				$attributeValue =  wptexturize( implode( ', ', $values ) );

				echo "<label>$attributeTitle:</label> <value>$attributeValue</value><br/>";
			endforeach;
		?>

		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>

	</a>

	<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		// NYX:DM 30.03.2015 hid "show more" button
		//do_action( 'woocommerce_after_shop_loop_item' ); 

	?>

</li>
